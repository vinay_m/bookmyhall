/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import Home from './component/home/home'
import { Provider } from 'react-redux';
import store from './component/redux/store';
// import SplashScreen from 'react-native-splash-screen';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

class App extends Component {
  componentDidMount() {
  }
  render() {
    return (
      <>
        <Provider store={store}>
          <Home />
        </Provider>
      </>
    );
  }
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default App;
