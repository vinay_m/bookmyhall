import { combineReducers } from 'redux';
import { HallReducer } from './hallReducer';

const reducers = combineReducers({
    allHalls: HallReducer,
})
export default reducers;