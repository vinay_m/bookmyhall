import { ActionType } from "../constants/action-types";

const initialState = {
    halls: [
        {
            id: 1,
            title: "Jayanth convention center",
            address1: "Nagarabhavi",
            address2: "Bangalore",
            image: [
                "https://media.weddingz.in/images/064d37e17398c87cb8323850d35fc6d8/jayanth-convention-center-nagarbhavi-bangalore.jpg",
                "https://cdn0.weddingwire.in/emp/fotos/4/8/0/4/r10_2x_22788781-378528609269289-5021866473196641006-n_15_134804.jpg",
                "https://image.wedmegood.com/resized/500X/uploads/member/1173180/1585831209_Screenshot_3.jpg",
                "https://content3.jdmagicbox.com/comp/kolar/t8/9999p8152.8152.170928091004.x1t8/catalogue/narayani-convention-hall-kolar-52n3anx0qd.jpg?clr=663300",
                "https://lh3.googleusercontent.com/felaXzDmYNIu-OrZPWbnT3THg7AIM1cCMiJP8IC16H0RbfZljZUnLVN_p-hsDctPvkek6Kvopt_VPpGWwQXfz5k=w746-h498-l95-e31",
                "https://storage.googleapis.com/showmyhall.appspot.com/hall_media/hall_index_2730/hall_pic__968a5dfe40499459b844d07e50fa845dedead10a",
            ],
            numbers: '100-500',
        },
        {
            id: 2,
            title: "MR Conventional Hall",
            address1: "Banshankari",
            address2: "Bangalore",
            image: [
                "https://media.weddingz.in/images/064d37e17398c87cb8323850d35fc6d8/jayanth-convention-center-nagarbhavi-bangalore.jpg",
                "https://cdn0.weddingwire.in/emp/fotos/4/8/0/4/r10_2x_22788781-378528609269289-5021866473196641006-n_15_134804.jpg",
                "https://image.wedmegood.com/resized/500X/uploads/member/1173180/1585831209_Screenshot_3.jpg",
                "https://content3.jdmagicbox.com/comp/kolar/t8/9999p8152.8152.170928091004.x1t8/catalogue/narayani-convention-hall-kolar-52n3anx0qd.jpg?clr=663300",
                "https://lh3.googleusercontent.com/felaXzDmYNIu-OrZPWbnT3THg7AIM1cCMiJP8IC16H0RbfZljZUnLVN_p-hsDctPvkek6Kvopt_VPpGWwQXfz5k=w746-h498-l95-e31",
                "https://storage.googleapis.com/showmyhall.appspot.com/hall_media/hall_index_2730/hall_pic__968a5dfe40499459b844d07e50fa845dedead10a",
            ],
            numbers: '100-500',
        },
        {
            id: 3,
            title: "Rathna (CMS) Conventional Hall",
            address1: "Banshankari",
            address2: "Bangalore",
            image: [
                "https://media.weddingz.in/images/064d37e17398c87cb8323850d35fc6d8/jayanth-convention-center-nagarbhavi-bangalore.jpg",
                "https://cdn0.weddingwire.in/emp/fotos/4/8/0/4/r10_2x_22788781-378528609269289-5021866473196641006-n_15_134804.jpg",
                "https://image.wedmegood.com/resized/500X/uploads/member/1173180/1585831209_Screenshot_3.jpg",
                "https://content3.jdmagicbox.com/comp/kolar/t8/9999p8152.8152.170928091004.x1t8/catalogue/narayani-convention-hall-kolar-52n3anx0qd.jpg?clr=663300",
                "https://lh3.googleusercontent.com/felaXzDmYNIu-OrZPWbnT3THg7AIM1cCMiJP8IC16H0RbfZljZUnLVN_p-hsDctPvkek6Kvopt_VPpGWwQXfz5k=w746-h498-l95-e31",
                "https://storage.googleapis.com/showmyhall.appspot.com/hall_media/hall_index_2730/hall_pic__968a5dfe40499459b844d07e50fa845dedead10a",
            ],
            numbers: '100-500',
        },
        {
            id: 4,
            title: "Narayani Conventional Hall",
            address1: "",
            address2: "Kolar",
            image: [
                "https://media.weddingz.in/images/064d37e17398c87cb8323850d35fc6d8/jayanth-convention-center-nagarbhavi-bangalore.jpg",
                "https://cdn0.weddingwire.in/emp/fotos/4/8/0/4/r10_2x_22788781-378528609269289-5021866473196641006-n_15_134804.jpg",
                "https://image.wedmegood.com/resized/500X/uploads/member/1173180/1585831209_Screenshot_3.jpg",
                "https://content3.jdmagicbox.com/comp/kolar/t8/9999p8152.8152.170928091004.x1t8/catalogue/narayani-convention-hall-kolar-52n3anx0qd.jpg?clr=663300",
                "https://lh3.googleusercontent.com/felaXzDmYNIu-OrZPWbnT3THg7AIM1cCMiJP8IC16H0RbfZljZUnLVN_p-hsDctPvkek6Kvopt_VPpGWwQXfz5k=w746-h498-l95-e31",
                "https://storage.googleapis.com/showmyhall.appspot.com/hall_media/hall_index_2730/hall_pic__968a5dfe40499459b844d07e50fa845dedead10a",
            ],
            numbers: '100-500',
        },
        {
            id: 5,
            title: "Swagath Grand Conventional Hall",
            address1: "Banshankari",
            address2: "Bangalore",
            image: [
                "https://media.weddingz.in/images/064d37e17398c87cb8323850d35fc6d8/jayanth-convention-center-nagarbhavi-bangalore.jpg",
                "https://cdn0.weddingwire.in/emp/fotos/4/8/0/4/r10_2x_22788781-378528609269289-5021866473196641006-n_15_134804.jpg",
                "https://image.wedmegood.com/resized/500X/uploads/member/1173180/1585831209_Screenshot_3.jpg",
                "https://content3.jdmagicbox.com/comp/kolar/t8/9999p8152.8152.170928091004.x1t8/catalogue/narayani-convention-hall-kolar-52n3anx0qd.jpg?clr=663300",
                "https://lh3.googleusercontent.com/felaXzDmYNIu-OrZPWbnT3THg7AIM1cCMiJP8IC16H0RbfZljZUnLVN_p-hsDctPvkek6Kvopt_VPpGWwQXfz5k=w746-h498-l95-e31",
                "https://storage.googleapis.com/showmyhall.appspot.com/hall_media/hall_index_2730/hall_pic__968a5dfe40499459b844d07e50fa845dedead10a",
            ],
            numbers: '100-500',
        },
        {
            id: 6,
            title: "Kara Conventional Hall",
            address1: "Nagarbhavi",
            address2: "Bangalore",
            image: [
                "https://media.weddingz.in/images/064d37e17398c87cb8323850d35fc6d8/jayanth-convention-center-nagarbhavi-bangalore.jpg",
                "https://cdn0.weddingwire.in/emp/fotos/4/8/0/4/r10_2x_22788781-378528609269289-5021866473196641006-n_15_134804.jpg",
                "https://image.wedmegood.com/resized/500X/uploads/member/1173180/1585831209_Screenshot_3.jpg",
                "https://content3.jdmagicbox.com/comp/kolar/t8/9999p8152.8152.170928091004.x1t8/catalogue/narayani-convention-hall-kolar-52n3anx0qd.jpg?clr=663300",
                "https://lh3.googleusercontent.com/felaXzDmYNIu-OrZPWbnT3THg7AIM1cCMiJP8IC16H0RbfZljZUnLVN_p-hsDctPvkek6Kvopt_VPpGWwQXfz5k=w746-h498-l95-e31",
                "https://storage.googleapis.com/showmyhall.appspot.com/hall_media/hall_index_2730/hall_pic__968a5dfe40499459b844d07e50fa845dedead10a",
            ],
            numbers: '100-500',
        },
    ],
}

export const HallReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ActionType.SET_HALL:
            return state;
        default:
            return state;
    }
}