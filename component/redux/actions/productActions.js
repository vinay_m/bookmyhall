import { ActionType } from '../constants/action-types';

export const setHall = (hall) => {
    return {
        type: ActionType.SET_HALL,
        payload: hall
    }
}
export const selectedHall = (hall) => {
    return {
        type: ActionType.SELECTED_HALL,
        payload: hall
    }
}