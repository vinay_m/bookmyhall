import React from 'react';
import { View, Text, FlatList, Image, Dimensions, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import { useSelector } from 'react-redux';
import { primaryColor, secondaryColor, tertiaryColor, fifthColor, fourthColor } from '../../../resuable/color'

const ImageList = (image) => {
    const img = image.images.map((i) => {
        return (
            <View style={{ padding: 5, }}>
                <Image source={{ uri: i }} style={{ height: 100, width: 170, borderRadius: 5 }} />
            </View>
        )
    })
    return img;
}

const ListOfHalls = () => {
    const data = useSelector((state) => state.allHalls.halls);
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    return (
        <View>
            <FlatList
                data={data}
                renderItem={({ item }) => {
                    console.log(item)
                    return (
                        <View style={styles.card}>
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}
                            >
                                <ImageList images={item.image} />
                            </ScrollView>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10 }}>
                                <View>
                                    <Text style={{ fontSize: 22 }}>{item.title}</Text>
                                    <Text style={{ fontSize: 16 }}>{item.address1}, {item.address2}</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text>Accomidation: </Text>
                                        <Text>{item.numbers}</Text>
                                    </View>
                                </View>
                                <View style={{ justifyContent: 'center', }}>
                                    <TouchableOpacity style={styles.button}><Text style={{ fontWeight: 'bold',color:'#666666' }}>View Details</Text></TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    )
                }}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    card: {
        // backgroundColor:'pink',
        padding: 5,
        marginVertical: 10
    },
    button: {
        backgroundColor: fourthColor,
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 5
    }
})
export default ListOfHalls;