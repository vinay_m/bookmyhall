import { View, Text, StatusBar, StyleSheet } from 'react-native';
import React, { Component } from 'react';
import { primaryColor, tertiaryColor } from '../../resuable/color';
import ListOfHalls from './halls/listOfHalls';
import { Searchbar } from 'react-native-paper';

const Home = () => {
    const [searchQuery, setSearchQuery] = React.useState('');

    const onChangeSearch = query => setSearchQuery(query);
    return (
        <>
            <StatusBar backgroundColor={tertiaryColor} barStyle="light-content" />
            <View style={styles.header}>
                <Searchbar 
                    placeholder="Search"
                    onChangeText={onChangeSearch}
                    value={searchQuery}
                />
            </View>
            <View style={styles.content}>
                <ListOfHalls />
            </View>
        </>
    )
}
const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    header: {
        backgroundColor: primaryColor,
        padding: 15
    },
    headerContent: {
        color: '#ffff',
        fontSize: 25,
        fontWeight: 'bold'
    }
})

export default Home;