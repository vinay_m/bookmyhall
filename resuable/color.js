export const primaryColor = '#e31e4f';
export const secondaryColor = '#e34c1e';
export const tertiaryColor = '#b5163d';
export const fourthColor = "#f2b93d";
export const fifthColor = "#e3cf1e";
